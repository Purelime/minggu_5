package com.renseki.app.projectpertamaku

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.renseki.app.projectpertamaku.model.Gender
import com.renseki.app.projectpertamaku.model.Mahasiswa
import kotlinx.android.synthetic.main.second_activity.*

class SecondActivity : AppCompatActivity() {

    var viewModel: SecondViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)

        viewModel = SecondViewModel()

        btn_add.setOnClickListener {
            val nrp = the_real_nrp.text.toString()
            val name = the_real_full_name.text.toString()
            val isMale = male.isChecked

            addToBottom(nrp, name, isMale)

            sendToAnotherActivity(nrp, name, isMale)
        }
    }

    private fun sendToAnotherActivity(
            nrp: String,
            name: String,
            isMale: Boolean
    ) {
        val intent = ThirdActivity.getStartIntent(
                this,
                Mahasiswa(
                        nrp = nrp,
                        name = name,
                        gender = if (isMale) Gender.MALE else Gender.FEMALE
                )
        )
        startActivity(intent)
    }

    private fun addToBottom(
            nrp: String,
            name: String,
            isMale: Boolean) {

        val newData = viewModel?.generateData(
                nrp,
                name,
                isMale
        )

        if (newData != null) {
            result.append("$newData\n")
        }
    }

    override fun onDestroy() {
        viewModel = null
        super.onDestroy()
    }
}
